'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
//main page controller
  .controller('barCtrl', ['$scope', function($scope) {
   var myConfig ={  
   graphset:[  
      {  
         type:"varea",
         height:"100%",
         width:"100%",
         x:"0%",
         y:"0%",
         series:[  
            {  
               values:[  20,40,25,50,15,45,33,34]
            }
         ]
      }
   ]
};

zingchart.render({ 
   id : 'myChart', 
   data : myConfig, 
   height: "100%", 
   width: "100%" 
});
  }]);
