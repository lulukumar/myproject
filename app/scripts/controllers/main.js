'use strict';

/**
 * @ngdoc function
 * @name clientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the clientApp
 */
angular.module('clientApp')
//main page controller
  .controller('MainCtrl', ['$scope', function($scope) {
       $scope.Navprofile =[
          {"Navlist": "Welcome Asit","icon":"fa-user"},
          {"Navlist" : "My Profile","icon":"fa-book"},
          {"Navlist": "Viewed Profile","icon":"fa-bullhorn"},
          {"Navlist" : "Latest Updates","icon":"fa-caret-square-o-down"},
          {"Navlist": "Profile Liked","icon":"fa-arrows"},
          {"Navlist" : "Logout","icon":"fa-bell"}
         ];
         $scope.userProfile ={
            "name" : "Welcome User"
         }
         $scope.bar = "User Bar Graph";
         $scope.line = "User Line Graph";
         $scope.todo= "To Do Tasks";
         $scope.service ="Services";
         $scope.alfa ="Alfa Projects";


       $scope.sidenav= [
         {"listitem" : "Dashboard","icon":"fa-th-large","state":"#dashboard"},
         {"listitem" : "Layouts","icon":"fa-cloud-download","state":"#layout"},
         {"listitem" : "Graphs","icon":"fa-bar-chart-o", "state" : "#graphs"},
         {"listitem" : "Mailbox","icon":"fa-envelope"},
         {"listitem" : "Metrics","icon":"fa-cube"},
         {"listitem" : "Widgets","icon":"fa-flask"},
         {"listitem" : "Forms","icon":"fa-edit"},
         {"listitem" : "App Views","icon":"fa-desktop"},
         {"listitem" : "Tables","icon":"fa-files-o"},
         {"listitem" : "Miscellaneous","icon":"fa-globe"},
       ];
      
       // angular chart js javascript srts here !!
       // $scope.append ="toggled";
       $scope.addTaxDtls = {
            items: [{
                "name" : ""
            }]
        };
        $scope.product =["Add My Number" , "Add My Email" , "Add My Fax"];
        $scope.addMoreTaxDtls = function(addMoreTaxDtls) {
            console.log("Hi asit", addMoreTaxDtls);
            if(addMoreTaxDtls !== undefined){
                $scope.addTaxDtls.items.push({
                     "name" : addMoreTaxDtls
                });
            }
           
        };
        $scope.removeTaxDtls = function(index) {
            $scope.addTaxDtls.items.splice(index, 1);
        };




        $scope.tableheading = "Row";
        $scope.tablefname = "First Name";
        $scope.tablelname = "Last Name";
        $scope.tableemail = "Email";
        $scope.tableaction = "Action";


        $scope.tabledata={
          "data" : [{
            "row" : "1",
            "firstname" : "Asit",
            "lastname": "Kumar",
            "email": "asit@holidaysimply.com",
            "action" : "fa-check"
          },
          {
            "row" : "2",
            "firstname" : "Narendra",
            "lastname": "Kumar",
            "email": "narendra@holidaysimply.com",
            "action" : "fa-check"
          },
          {
            "row" : "3",
            "firstname" : "Rangrajan",
            "lastname": "Kumar",
            "email": "rangrajan@holidaysimply.com",
            "action" : "fa-check"
          },
          {
            "row" : "4",
            "firstname" : "Indrajit",
            "lastname": "Kumar",
            "email": "indrajit@holidaysimply.com",
            "action" : "fa-check"
          },
          ]
        };

  }]);

  

  
