'use strict';

/**
 * @ngdoc overview
 * @name clientApp
 * @description
 * # clientApp
 *
 * Main module of the application.
 */
angular
  .module('clientApp', [ 
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'zingchart-angularjs',
    'ngSanitize',
    'ngTouch',
    

  ])

   // .controller('MainController', function($scope, $route, $routeParams, $location) {
   //     $scope.$route = $route;
   //     $scope.$location = $location;
   //     $scope.$routeParams = $routeParams;
   // })

  .config(function ($routeProvider) {
    $routeProvider
      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'MainCtrl'
      })


      $routeProvider
      .when('/layout', {
        templateUrl: 'views/layout.html',
        controller: 'MainCtrl'
      })

      $routeProvider
      .when('/graphs', {
        templateUrl: 'views/graphs.html',
        controller: 'barCtrl'
      })

      $routeProvider
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'barCtrl'
      })
      
      
      .otherwise({
        redirectTo: '/login'
      });
  });
